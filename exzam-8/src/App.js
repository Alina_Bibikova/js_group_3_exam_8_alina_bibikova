import React, {Component, Fragment} from 'react';
import './App.css';
import QuotesList from "./containers/QuotesList/QuotesList";
import {NavLink as RouterNavLink} from "react-router-dom";
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {Route, Switch} from "react-router";
import AddQuotes from "./containers/AddQuotes/AddQuotes";
import EditQuotes from "./containers/EditQuotes/EditQuotes";
import DeleteQuotes from "./containers/DeleteQuotes/DeleteQuotes";

class App extends Component {
  render() {
    return (
        <Fragment>
            <Navbar color="light" light expand="md">
                <NavbarBrand tag={RouterNavLink} to="/" exact>Quotes Central</NavbarBrand>
                <NavbarToggler/>
                <Collapse isOpen navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/" exact>Quotes</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/add">Submit new quote</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
            <Container>
                <Switch>
                    <Route path="/" exact component={QuotesList} />
                    <Route path="/add" exact component={AddQuotes} />
                    <Route path="/quotes/:id/edit" component={EditQuotes}/>
                    <Route path="/quotes/:id/delete" component={DeleteQuotes}/>
                    <Route path="/quotes/:categoryId" component={QuotesList}/>
                    <Route render={() => <h1>Hot found</h1>} />
                </Switch>
            </Container>
        </Fragment>
    );
  }
}

export default App;
