import React, {Component} from 'react';
import {CATEGORIES} from "../../constans";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
class QuotesForm extends Component {

    constructor(props) {
        super(props);

        if (props.quotes) {
            this.state = {...props.quotes};
        } else {
            this.state = {
                category: Object.keys(CATEGORIES)[0],
                author: '',
                quoteText: '',
            };
        }
    }

    valueChanged = event => {
        const {name, value} = event.target;

        this.setState({[name]: value});
    };

    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <Form className="QuotesForm" onSubmit={this.submitHandler}>
                <FormGroup row>
                    <Label for="category" sm={2}>Category</Label>
                    <Col sm={10}>
                        <Input type="select" name="category" id="category" onChange={this.valueChanged} value={this.state.category}>
                            {Object.keys(CATEGORIES).map(categoryId => (
                                <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="category" sm={2}>Author</Label>
                    <Col sm={10}>
                        <Input type="text" name="author" id="author"  placeholder="Author"
                               value={this.state.author} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="category" sm={2}>Quote text</Label>
                    <Col sm={10}>
                        <Input type="textarea" name="quoteText" placeholder="Quote text"
                               value={this.state.quoteText} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{size: 10, offset: 2}}>
                        <Button color="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default QuotesForm;