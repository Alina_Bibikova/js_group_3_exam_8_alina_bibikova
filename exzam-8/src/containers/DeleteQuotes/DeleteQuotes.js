import React, {Component, Fragment} from 'react';
import axios from "../../axios-citation";
import DeleteForm from "../../components/DeleteForm/DeleteForm";

class DeleteQuotes extends Component {

    state = {
        quotes: null
    };

    getQuotesUrl = () => {
        const id = this.props.match.params.id;
        return 'quotes/' + id + '.json';
    };

    componentDidMount() {

        axios.get(this.getQuotesUrl()).then(response => {
            this.setState({quotes: response.data});
        })
    }

    deleteQuotes = () => {

        axios.delete('/quotes/' + this.props.match.params.id + '.json').finally(() => {
            this.props.history.push('/');
        });
    };

    render() {
            let form = <DeleteForm
            onSubmit={this.deleteQuotes}
            quotes={this.state.quotes}
        />;

        if (!this.state.quotes) {
            form = <div>Loading...</div>
        }
        return (
            <Fragment>
                <h1>Delete quotes</h1>
                {form}
            </Fragment>
        );
    }
}

export default DeleteQuotes;