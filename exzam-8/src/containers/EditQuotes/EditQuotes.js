import React, {Component, Fragment} from 'react';
import QuotesForm from "../../components/QuotesForm/QuotesForm";
import axios from '../../axios-citation';

class EditQuotes extends Component {

    state = {
        quotes: null
    };

    getQuotesUrl = () => {
        const id = this.props.match.params.id;
        return 'quotes/' + id + '.json';
    };

    componentDidMount() {

        axios.get(this.getQuotesUrl()).then(response => {
            this.setState({quotes: response.data});
        })
    }

    editQuotes = quotes  => {
        axios.put(this.getQuotesUrl(), quotes).then(() => {
            this.props.history.goBack();
        });
    };

    render() {
            let form = <QuotesForm
            onSubmit={this.editQuotes}
            quotes={this.state.quotes}
        />;

        if (!this.state.quotes) {
            form = <div>Loading...</div>
        }
        return (
            <Fragment>
                <h1> Edit quotes</h1>
                {form}
            </Fragment>
        );
    }
}

export default EditQuotes;