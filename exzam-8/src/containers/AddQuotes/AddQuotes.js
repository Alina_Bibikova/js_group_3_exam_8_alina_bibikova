import React, {Component, Fragment} from 'react';
import QuotesForm from "../../components/QuotesForm/QuotesForm";
import axios from '../../axios-citation';

class AddQuotes extends Component {

    addQuotes = quotes => {
        axios.post('quotes.json', quotes).then(() => {
            this.props.history.replace('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h1>Add new quotes</h1>
                <QuotesForm onSubmit={this.addQuotes}/>
            </Fragment>
        );
    }
}

export default AddQuotes;