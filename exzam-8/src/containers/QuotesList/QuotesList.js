import React, {Component} from 'react';
import {NavLink as RouterNavLink} from "react-router-dom";
import {Button, Card, CardBody, CardFooter, CardText, CardTitle, Col, Nav, NavItem, NavLink, Row} from "reactstrap";
import axios from '../../axios-citation';
import {CATEGORIES} from "../../constans";
import './QuotesList.css';

class QuotesList extends Component {

    state = {
       quotes: null
    };

    loadData() {
        let url = 'quotes.json';

        const categoryId = this.props.match.params.categoryId;

        if(categoryId) {
            url += `?&orderBy="category"&equalTo="${categoryId}"`;
        }

        axios.get(url).then(response => {
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id};
            });

            this.setState({quotes: quotes});
        });
    }

    componentDidMount () {
        this.loadData();
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.categoryId !== prevProps.match.params.categoryId){
            this.loadData()
        }
    }

    render() {

        let quotes = null;

        if(this.state.quotes) {
            console.log(this.state.quotes);
            quotes = this.state.quotes.map(quotes => (
                <Card key={quotes.id}>
                    <CardBody>
                        <CardTitle>{quotes.author}</CardTitle>
                        <CardText>{quotes.quoteText}</CardText>
                    </CardBody>
                    <CardFooter>
                        <RouterNavLink to={'/quotes/' + quotes.id + '/edit'}>
                            <Button color="primary">Edit</Button>
                        </RouterNavLink>
                        <RouterNavLink to={'/quotes/' + quotes.id + '/delete'}>
                            <Button color="danger">Delete</Button>
                        </RouterNavLink>
                    </CardFooter>
                </Card>
            ));
        }

        return (
            <Row style={{marginTop: "20px"}}>

                <Col sm={3}>
                    <Nav vertical>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/" exact>All quotes</NavLink>
                        </NavItem>
                        {Object.keys(CATEGORIES).map(categoryId => (
                            <NavItem key={categoryId}>
                                <NavLink
                                    tag={RouterNavLink}
                                    to={"/quotes/" + categoryId}
                                    exact
                                >
                                    {CATEGORIES[categoryId]}</NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                </Col>

                <Col sm={9}>
                    {quotes}
                </Col>
            </Row>
        );
    }
}

export default QuotesList;